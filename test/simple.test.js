const test = require('tape')
const Reduce = require('../')

const Strategy = require('@tangle/strategy')
const Overwrite = require('@tangle/overwrite')
const SimpleSet = require('@tangle/simple-set')

const strategy = new Strategy({
  title: Overwrite()
})

test('reduce (simple)', t => {
  //   A   (root)
  //   |
  //   B

  const A = {
    key: 'A',
    data: { author: '@mix' },
    previous: null
  }
  const B = {
    key: 'B',
    data: {
      author: '@stranger',
      title: { set: 'nice nice' }
    },
    previous: ['A']
  }

  t.deepEqual(
    new Reduce(strategy, { nodes: [A] }).state,
    {
      A: {
        title: {}
      }
    }
  )

  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B] }).state,
    {
      B: {
        title: { set: 'nice nice' }
      }
    }
  )

  t.deepEqual(new Reduce(strategy).state, {}, 'can reduce an empty graph')

  t.end()
})

test('reduce (branches)', t => {
//    A
//   /  \
//  B   C
//      /\
//     D  E
  const menuStrategy = new Strategy({
    title: Overwrite(),
    menu: SimpleSet()
  })

  const A = {
    key: 'A',
    data: {
      author: '@colin',
      title: { set: 'What are people bringing to potluck?' }
    },
    previous: null
  }

  const B = {
    key: 'B',
    data: {
      author: '@rudeperson',
      menu: { pizza: 1 }
    },
    previous: ['A']
  }
  const C = {
    key: 'C',
    data: {
      author: '@glutenintolerant',
      menu: { salad: 1 },
      comment: 'Nothing with gluten'
    },
    previous: ['A']
  }
  const D = {
    key: 'D',
    data: {
      author: '@rudeperson',
      menu: { pizza: -1 },
      comment: 'whoops'
    },
    previous: ['C']
  }
  const E = {
    key: 'E',
    data: {
      author: '@someoneelse',
      title: { set: 'final menu' },
      menu: { drinks: 1 }
    },
    previous: ['C']
  }
  console.warn('Start test')
  // Testing how the tangle reduces with branching nodes
  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A] }).state,
    {
      A: {
        title: { set: 'What are people bringing to potluck?' },
        menu: {} // Note that menu is included even though it was not in A.
      }
    },
    'Just the root'
  )

  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A, B] }).state,
    {
      B: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { pizza: 1 }
      }
    },
    'AB: a simple reduce that adds to the menu'
  )
  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A, B, C] }).state,
    {
      B: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { pizza: 1 }
      },
      C: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { salad: 1 }
      }
    },
    'ABC: A branch with two tips that add to menu'
  )

  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A, B, C, D, E] }).state,
    {
      B: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { pizza: 1 }
      },
      D: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { salad: 1, pizza: -1 }
      },
      E: {
        title: { set: 'final menu' },
        menu: { salad: 1, drinks: 1 }
      }
    },
    'ABCDE: Reduces whole graph. Changes title and removes item from menu'
  )

  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A, C, E] }).state,
    {
      E: {
        title: { set: 'final menu' },
        menu: { salad: 1, drinks: 1 }
      }
    },
    'ACE, missing the branch nodes'
  )

  t.deepEqual(
    new Reduce(menuStrategy, { nodes: [A, B, D, E] }).state,
    {
    // Ignore nodes that are unconnected
      B: {
        title: { set: 'What are people bringing to potluck?' },
        menu: { pizza: 1 }
      }
    },
    'ABDE, missing connecting node C'
  )
  // After reducing the graph we can try to use strategy.mapToOutput
  // But there may be multiple nodes after reducing
  t.deepEqual(
    menuStrategy.mapToOutput((new Reduce(menuStrategy, { nodes: [A, B, C, D, E] }).state).E),
    {
      title: 'final menu',
      menu: ['drinks', 'salad']
    }
    ,
    'Reify E in ABCDE'
  )

  t.end()
})
