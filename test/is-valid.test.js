const test = require('tape')
const Reduce = require('../')

const Strategy = require('@tangle/strategy')
const Overwrite = require('@tangle/overwrite')
const SimpleSet = require('@tangle/simple-set')

const strategy = new Strategy({
  title: Overwrite(),
  otherAuthors: SimpleSet()
  // NOTE - recommend using complex-set in production so you can track when an author was added
})

function isValidNextStep (context, nextNode) {
  const { tips, graph } = context

  if (!tips) throw new Error('must have initial accT')

  const isAuthor = graph.rootNodes.some(rootNode => rootNode.data.author === nextNode.data.author)
  if (isAuthor) return true

  const isOtherAuthor = tips.every(tip => {
    return tip.T.otherAuthors[nextNode.data.author] > 0
    // look at the current tips, to see if the accumulated Transformation state (tip.T)
    // for that node includes this new nodes author in the list of approved authors

    // NOTE this is janky, but works for this test.
    // DO NOT copy this to a production app!
  })
  return isOtherAuthor
}

test('opts.isValidNextStep', t => {
  //   A   (root)
  //   |
  //   B
  //   |
  //   C

  const A = {
    key: 'A',
    data: {
      author: '@mix',
      title: { set: 'my root message' }
    },
    previous: null
  }
  const B = {
    key: 'B',
    data: {
      author: '@cherese',
      title: { set: 'nice nice' }
    },
    previous: ['A']
  }
  const C = {
    key: 'C',
    data: {
      author: '@mix',
      title: { set: 'nice nice!!!!' }
    },
    previous: ['B']
  }
  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C], isValidNextStep }).state,
    {
      A: {
        title: { set: 'my root message' },
        otherAuthors: {}
      }
    },
    'ignores invalid nodes'
  )

  const X = {
    key: 'X',
    data: {
      author: '@mix',
      title: { set: 'my root message' },
      otherAuthors: { '@cherese': 1 }
    },
    previous: null
  }
  const Y = {
    key: 'Y',
    data: {
      author: '@cherese',
      title: { set: 'nice nice' }
    },
    previous: ['X']
  }

  t.deepEqual(
    new Reduce(strategy, { nodes: [X, Y], isValidNextStep }).state,
    {
      Y: {
        title: { set: 'nice nice' },
        otherAuthors: { '@cherese': 1 }
      }
    },
    'includes valid nodes'
  )

  t.end()
})

// questions:
// - should it just perform an additional check before / after basic tangle validity checks have been made?
// - is this where "is time-traveller" check would be made?
//     - if so might need "nodes so far" / "path" + lookup
//     - mind you reverseMap should get us nodes so far if needed, so perhaps only the lookup + reverseMap
//     - or the graph object
