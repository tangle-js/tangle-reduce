const Graph = require('@tangle/graph')
const Queue = require('./queue')

module.exports = class Reduce {
  constructor (strategy, opts = {}) {
    this.strategy = strategy

    const {
      nodes = [],
      isValidNextStep
    } = opts
    // TODO prune time-travellers
    this.graph = new Graph(nodes)
    this.isValidNextStep = isValidNextStep
    this.getT = (key, distance) => {
      return strategy.mapToPure(this.graph.getNode(key).data, distance)
    }

    this._resolvedState = null
  }

  get state () {
    return this.resolve()
  }

  resolve () {
    if (this._resolvedState) return this._resolvedState

    const { graph, strategy, getT, isValidNextStep } = this

    const tips = {
      queue: new Queue(),
      // nodes we're processing to find tips, made up of objects { key, T, d }, representing position in graph:
      //   - key = the unique identifier for a node
      //   - T = the accumulated (concat'd) Transformation up to and including node `key`
      //   - d = distance from root, in a merge it increases to be longer than the longest path
      preMerge: new Map(),
      // a store for nodes from queue which are waiting for other nodes preceding a merge node being resolved,
      // these are held till all required nodes and states are ready
      // {
      //   [key]: { key, T, d },
      // }
      //
      terminal: {}
      // maps key -> T
    }

    if (isValidNextStep) {
      const initialContext = {
        tips: [{ T: strategy.identity() }],
        graph
      }

      graph.rootNodes.forEach(node => {
        if (!isValidNextStep(initialContext, node)) {
          throw new Error('tangle starting node invalid!')
        }
      })
    }

    // seed the queue with starting node(s)
    graph.rootNodeKeys.forEach(key => {
      tips.queue.add({
        key,
        T: getT(key, 0),
        d: 0
      })
    })

    while (!tips.queue.isEmpty()) {
      const { key, T, d } = tips.queue.next()
      // T is the accumulated Transformation so far
      // (NOT including Transformation stored in key though, that's what we're )

      if (isValidNextStep) {
      // check if the next steps are valid before taking them
      // prune here, because this might change which nodes are new tips / tips
        graph.getLinks(key).forEach(nextKey => {
          const tangleContext = {
            tips: [{ key, T, d }], // we use "key" here for consistency
            graph
          }
          const nextNodeValid = isValidNextStep(tangleContext, graph.getNode(nextKey))
          if (!nextNodeValid) graph.invalidateKeys([nextKey])
        })
      }

      if (graph.isTipNode(key)) {
        tips.terminal[key] = T
        continue
      }

      graph.getLinks(key).forEach(nextKey => {
        if (!graph.isMergeNode(nextKey)) {
          tips.queue.add({
            key: nextKey,
            T: strategy.concat(T, getT(nextKey, d + 1)),
            d: d + 1
          })
        // queue up the another node to explore from
        } else {
          tips.preMerge.set(key, { key, T, d })

          const requiredNodeIds = graph.getBacklinks(nextKey)
          const isMergeReady = requiredNodeIds.every(key => tips.preMerge.has(key))
          // check tips.preMerge store to see if we now have the state needed to complete merge

          if (isMergeReady) {
            const maxD = requiredNodeIds
              .map(key => tips.preMerge.get(key).d)
              .reduce((acc, d) => d > acc ? d : acc, 0)

            const mergeNode = this.graph.getNode(nextKey)
            if (strategy.isValidMerge(graph, mergeNode)) {
              tips.queue.add({
                key: nextKey,
                T: strategy.merge(graph, mergeNode),
                d: maxD + 1
              })
            } else {
              // The next node is not a valid merge
              graph.invalidateKeys([nextKey])
              // Need to check if tips.premerge nodes are now terminal
              requiredNodeIds.forEach(prevId => {
                if (graph.isTipNode(prevId)) {
                  tips.terminal[prevId] = tips.preMerge.get(prevId).T
                }
              })
            }
          }
        }
      })
    }

    this._resolvedState = tips.terminal
    return this._resolvedState
  }

  addNodes (nodes) {
    this._resolvedState = null
    this.graph.addNodes(nodes)
    // idea: preserve the more state from initial resolve and find a way to re-ignite the the resolving path
    // This is challenging because you don't know which new nodes attach where....
  }
}
